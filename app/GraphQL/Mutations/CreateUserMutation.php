<?php


namespace App\GraphQL\Mutations;


use App\Entities\User;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class CreateUserMutation extends Mutation
{
    protected $attributes = [
        'name' => 'CreateUser'
    ];

    public function type(): Type
    {
        return GraphQL::type('user');
    }

    public function args(): array
    {
        return [
            'name' => ['name' => 'name', 'type' => Type::string()],
            'email' => ['name' => 'email', 'type' => Type::string()],
            'password' => ['name' => 'password', 'type' => Type::string()]
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'email' => ['required', 'email'],
            'name' => ['required', 'string' , 'min:2'],
            'avatar' => ['string' , 'min:10'],
            'password' => ['required', 'string' , 'min:4']
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo)
    {
        $user = new User();
        $user->name = $args['name'];
        $user->email = $args['email'];
        $user->avatar = $args['avatar']??null;
        $user->password = bcrypt( $args['password']);
        $user->save();

        return $user;
    }
}
