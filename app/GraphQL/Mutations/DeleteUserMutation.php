<?php


namespace App\GraphQL\Mutations;


use App\Entities\User;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class DeleteUserMutation extends Mutation
{
    protected $attributes = [
        'name' => 'DeleteUser'
    ];

    public function type(): Type
    {
        return GraphQL::type('user');
    }

    public function args(): array
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'id' => ['required'],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo)
    {
        $user = User::find($args['id']);
        if (!$user) {
            return null;
        }

        $user->delete();

        return $user;
    }
}
