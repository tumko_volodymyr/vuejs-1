<?php


namespace App\GraphQL\Mutations;


use App\Entities\User;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class UpdateUserNameMutation extends Mutation
{
    protected $attributes = [
        'name' => 'UpdateUserName'
    ];

    public function type(): Type
    {
        return GraphQL::type('user');
    }

    public function args(): array
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
            'name' => ['name' => 'name', 'type' => Type::string()]
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'id' => ['required'],
            'name' => ['required', 'string' , 'min:2']
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo)
    {
        $user = User::find($args['id']);
        if (!$user) {
            return null;
        }

        $user->name = $args['name'];
        $user->save();

        return $user;
    }
}
