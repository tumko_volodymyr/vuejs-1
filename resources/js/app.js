/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('app', require('./components/App.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import store from './store';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css'

import { library } from '@fortawesome/fontawesome-svg-core';
import {
    faHeart,
    faAngleDown,
    faComments,
    faShare,
    faUpload,
    faSignOutAlt,
    faCog
} from '@fortawesome/free-solid-svg-icons';
import {
    faTwitter,
    faTwitterSquare
} from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(
    faHeart,
    faAngleDown,
    faComments,
    faShare,
    faTwitter,
    faUpload,
    faSignOutAlt,
    faCog,
    faTwitterSquare,
    faUpload,
);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.config.productionTip = false;

Vue.use(Buefy, {
    defaultIconComponent: 'font-awesome-icon'
});

const app = new Vue({
    el: '#app',
    store
});
