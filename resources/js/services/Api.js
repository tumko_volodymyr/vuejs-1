import axios from 'axios/index';
class Api {
    constructor(apiUrl) {
        this.axios = axios.create({baseURL: apiUrl});
    }

    getUsers() {
        const data = {
            query: `
                {
                    users {
                        id
                        name
                        email
                        avatar
                    }
                }
            `
        };
        return handleResponse(this.axios.post('/graphql', data)).then(
            data => data.users
        );
    }

    getUserById(id) {
        const data = {
            query: `query GetUser($id: Int)
                {
                    users (id: $id) {
                        id
                        name
                        email
                        avatar
                    }
                }
            `,
            variables: {
                id: id
            }
        };
        return handleResponse(this.axios.post('/graphql', data)).then(
            data => data.users
        );
    }

    async deleteUserById(id) {
        const data = {
            query: `mutation deleteUser($id: Int)
                {
                    deleteUser (id: $id) {
                        id
                        name
                        email
                        avatar
                    }
                }
            `,
            variables: {
                id: id
            }
        };
        return handleResponse(this.axios.post('/graphql', data)).then(
            data => data.deleteUser
        );
    }

    async addUser({email, name, password}) {
        const data = {
            query: `mutation createUser($email: String, $name: String, $password: String)
                {
                    createUser (email: $email, name: $name, password: $password) {
                        id
                        name
                        email
                        avatar
                    }
                }
            `,
            variables: {
                email: email,
                name: name,
                password: password
            }
        };
        return handleResponse(this.axios.post('/graphql', data)).then(
            data => data.createUser
        );
    }

    async updateUserName({id, name}) {
        const data = {
            query: `mutation updateUserName($id: Int, $name: String)
                {
                    updateUserName (id: $id, name: $name) {
                        id
                        name
                        email
                        avatar
                    }
                }
            `,
            variables: {
                id: id,
                name: name,
            }
        };
        return handleResponse(this.axios.post('/graphql', data)).then(
            data => data.updateUserName
        );
    }

}

function handleResponse(promise) {
    return promise.then(
        response => {
            const errors = response.data.errors;
            if (errors !== undefined){
                return Promise.reject(new Error('Check input data'));

            }
            return response.data.data
        },
        errorResponse => {
            const { response } = errorResponse;

            if (!response) {
                return Promise.reject(new Error('Unexpected error!'));
            }

            const error = response.data.errors[0];
            return Promise.reject(error);
        }
    )


}

export default new Api('http://localhost:8000');
