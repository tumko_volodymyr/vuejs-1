import api from '../services/Api';
import {SET_USER, SET_USERS, ADD_USER, DELETE_USER} from './mutationTypes';

export default {
    async fetchUsers({ commit }) {
        try {
            const users = await api.getUsers();

            commit(SET_USERS, users);
            return Promise.resolve(users);
        } catch (error) {
            return Promise.reject(error);
        }
    },

    async addUser({ commit }, { email, name, password }) {

        try {
            const user = await api.addUser({email,name, password});
            commit(ADD_USER, user);
            return Promise.resolve(user);
        } catch (error) {
            return Promise.reject(error);
        }
    },

    async deleteUser({ commit }, id) {
        try {
            await api.deleteUserById(id);

            commit(DELETE_USER, id);
            return Promise.resolve();
        } catch (error) {
            return Promise.reject(error);
        }
    },

    async updateUserName({ commit }, { id, name }) {
        try {
            const user = await api.updateUserName({id, name});

            commit(SET_USER, user);
            return Promise.resolve(user);
        } catch (error) {
            return Promise.reject(error);
        }
    },
};
