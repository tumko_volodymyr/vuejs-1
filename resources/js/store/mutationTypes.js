export const SET_USERS = 'setUsers';
export const SET_USER = 'editUser';
export const ADD_USER = 'addUser';
export const DELETE_USER = 'deleteUser';
